from enum import IntEnum


class Status(IntEnum):
    pending = 10
    accepted = 20
    packed = 30
    shipped = 40
    completed = 50
    canceled = 60
