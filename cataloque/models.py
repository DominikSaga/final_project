from django.db.models import Model, CharField, SlugField, ForeignKey, CASCADE, TextField, ImageField, DecimalField, \
    PositiveIntegerField, DateTimeField, SmallIntegerField, EmailField, PROTECT
from cataloque.enums import Status


class Address(Model):
    street = CharField(max_length=128, blank=False, null=False)
    house_number = CharField(max_length=16, null=False, blank=False)
    country_iso3 = CharField(max_length=3, blank=False, null=False)
    zip_code = CharField(max_length=16, null=False, blank=False)


class User(Model):
    first_name = CharField(max_length=64, null=False, blank=False)
    last_name = CharField(max_length=64, null=False, blank=False)
    login = EmailField(max_length=128, null=False, blank=False)
    password = CharField(max_length=64, null=False, blank=False)
    permanent_address = ForeignKey(Address, on_delete=PROTECT, null=True)
    #role =


class Category(Model):
    name = CharField(max_length=128, null=False, blank=False)
    slug = SlugField(max_length=128, unique=True)

    def __str__(self):
        return self.name


class Product(Model):
    category = ForeignKey(Category, on_delete=CASCADE)
    title = CharField(max_length=128, null=False, blank=False)
    description = TextField(null=False, blank=False)
    price = DecimalField(max_digits=8, decimal_places=2)
    product_image = ImageField()

    def __str__(self):
        return self.title


class Cart(Model):
    user = ForeignKey(User, on_delete=CASCADE)
    product = ForeignKey(Product, on_delete=CASCADE)
    quantity = PositiveIntegerField()
    created_at = DateTimeField(auto_now_add=True)


class Order(Model):
    user = ForeignKey(User, on_delete=CASCADE)
    total_cost = DecimalField(max_digits=8, decimal_places=2)
    address = ForeignKey(Address, on_delete=CASCADE)
    amount = PositiveIntegerField()
    submission_date = DateTimeField(auto_now_add=True)
    status = SmallIntegerField(
        choices=((Status.pending.value, Status.pending.name), (Status.accepted.value, Status.accepted.name),
                 (Status.packed.value, Status.packed.name), (Status.shipped.value, Status.shipped.name),
                 (Status.completed.value, Status.completed.name), (Status.canceled.value, Status.canceled.name)))
